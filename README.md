# pagemarks-server

A Pagemarks server for self-hosting


## Run development server

- `virtualenv .venv`
- `.venv\scripts\activate.bat` (Windows) or `. .venv/bin/activate` (Linux/UNIX)
- `poetry install`
- `pagemarks serve`


## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License, version 3,
as published by the Free Software Foundation (SPDX ID: `GPL-3.0-only`).  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/gpl.html>.

Copyright (c) The Pagemarks contributors


## Project status

UNDER CONSTRUCTION - DO NOT USE YET
