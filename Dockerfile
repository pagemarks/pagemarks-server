#
# pagemarks-server - A Pagemarks server for self-hosting
# Copyright (c) The Pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

FROM python:3.13-alpine

LABEL org.opencontainers.image.authors="Barfuin <oss@barfuin.org>"
LABEL org.opencontainers.image.source="https://gitlab.com/pagemarks/pagemarks-server"
LABEL org.opencontainers.image.title="Pagemarks"
LABEL org.opencontainers.image.documentation="https://pagemarks.org"
# org.opencontainers.image.revision via Kaniko args
# org.opencontainers.image.version via Kaniko args

COPY dist/pagemarks-*.tar.gz /

RUN  apk update && apk add git && \
     pip install /pagemarks-*.tar.gz
