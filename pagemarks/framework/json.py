#
# pagemarks-server - A Pagemarks server for self-hosting
# Copyright (c) The Pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

from json import dumps, JSONEncoder
from typing import Optional



class JsonTagList:
    tag_list = None


    def __init__(self, tl):
        self.tag_list = tl



class JsonTagListEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, JsonTagList):
            return "##<{}>##".format(obj.tag_list).replace('\'', '"')



def clean_nones(record: dict) -> dict:
    """Remove the None values from the given dictionary."""
    return {
        key: val
        for key, val in record.items()
        if val is not None
    }



def to_json(record: dict, indent: Optional[int] = 4) -> str:
    doc = dumps(clean_nones(record), indent=indent, cls=JsonTagListEncoder, ensure_ascii=False)
    doc = doc.replace('"##<', '').replace('>##"', '')
    lines = doc.split('\n')
    doc = ''
    for line in lines:
        if line.strip().startswith('"tags":'):
            doc += line.replace('\\"', '"') + '\n'
        else:
            doc += line + '\n'
    return doc
