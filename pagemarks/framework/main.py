#
# pagemarks-server - A Pagemarks server for self-hosting
# Copyright (c) The Pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

import logging
from importlib.metadata import version
from sys import exit, stderr, stdout

import click
from flask import Flask
from flask.cli import FlaskGroup

from pagemarks.commands.build import CmdBuild
from pagemarks.commands.importer import CmdImport
from pagemarks.commands.init import CmdInit
from pagemarks.commands.locate import CmdLocate
from pagemarks.framework.cmdline import CmdLine
from pagemarks.framework.globals import DEFAULT_COLL_NAME


pass_config = click.make_pass_decorator(CmdLine, ensure=True)



class PagemarksLogFormatter(logging.Formatter):
    dbg_fmt = "DEBUG:%(module)s:%(lineno)d: %(msg)s"
    info_fmt = '%(msg)s'


    def __init__(self):
        super().__init__(fmt="%(levelname)s: %(msg)s", datefmt=None, style='%')


    def format(self, record):
        if record.levelno == logging.DEBUG or record.levelno == logging.INFO:
            # noinspection PyProtectedMember
            format_orig = self._style._fmt

            if record.levelno == logging.DEBUG:
                self._style._fmt = self.dbg_fmt
            elif record.levelno == logging.INFO:
                self._style._fmt = self.info_fmt

            result = logging.Formatter.format(self, record)
            self._style._fmt = format_orig
        else:
            result = logging.Formatter.format(self, record)
        return result



def init_logging(quiet: bool, verbose: bool) -> None:
    handler_err = logging.StreamHandler(stderr)
    handler_err.setLevel(logging.ERROR)
    handler_err.setFormatter(PagemarksLogFormatter())

    handler_out = logging.StreamHandler(stdout)
    handler_out.setLevel(logging.DEBUG)
    handler_out.addFilter(lambda record: record.levelno < logging.ERROR)
    handler_out.setFormatter(PagemarksLogFormatter())

    log_level = logging.INFO
    if verbose:
        log_level = logging.DEBUG
    elif quiet:
        log_level = logging.ERROR
    logging.basicConfig(handlers=[handler_out, handler_err], level=log_level)



@click.group()
@click.option('--repo', type=click.types.Path(), required=False, help='Path to pagemarks repository')
@click.option('--quiet', '-q', is_flag=True, help='Suppress all output except errors and command results')
@click.option('--verbose', is_flag=True, help='Enable debug output')
@click.option('--yes', '-y', is_flag=True, help='Answer all prompts with \'yes\'')
@click.version_option(version('pagemarks'), prog_name='pagemarks', message='%(prog)s v%(version)s')
@pass_config
def app(config: CmdLine, repo, quiet: bool, verbose: bool, yes: bool) -> None:
    """pagemarks.org: Free, git-backed, self-hosted bookmarks"""
    config.repo_dir = repo
    config.yes = yes
    init_logging(quiet, verbose)



@app.command('build')
@click.option('--output-dir', '-o', required=False, default='public',
        help='output directory for generated files')
@pass_config
def cmd_build(config: CmdLine, output_dir: str) -> None:
    """Build the downloadable bookmark data from your repository"""
    rc = CmdBuild(config, output_dir).run_command()
    if rc != 0:
        exit(rc)



@app.command('init')
@click.option('--collection', '-c', type=click.types.STRING, default=DEFAULT_COLL_NAME, required=False,
        help='The name of the initial collection to create')
@click.option('--user-name', required=False, type=click.types.STRING, default=None,
        help='User name to set in Git local config for the new repo')
@click.option('--user-email', required=False, type=click.types.STRING, default=None,
        help='User email to set in Git local config for the new repo')
@pass_config
def cmd_init(config: CmdLine, collection: str, user_name: str, user_email: str) -> None:
    """Initialize a new Pagemarks repository"""
    rc = CmdInit(config, collection, user_name, user_email).run_command()
    if rc != 0:
        exit(rc)



@app.command('locate')
@click.option('--collection', '-c', type=click.STRING, default=None, required=False,
        help='The name of the collection to operate on. Defaults to all collections')
@click.argument('url', required=True)
@pass_config
def cmd_locate(config: CmdLine, collection: str, url: str) -> None:
    """Locate the bookmark file for the given URL"""
    rc = CmdLocate(config, collection, url).run_command()
    if rc != 0:
        exit(rc)



@app.command('import')
@click.option('--collection', '-c', type=click.STRING, default=DEFAULT_COLL_NAME, required=False,
        help='The name of the collection to import into. Defaults to \'' + DEFAULT_COLL_NAME + '\'')
@click.option('--no-private', required=False, is_flag=True, help='Exclude bookmarks flagged as \'private\'')
@click.argument('input_path', required=True)
@pass_config
def cmd_import(config: CmdLine, collection: str, no_private: bool, input_path: str) -> None:
    """Import from an nb repository or from a Netscape bookmarks file"""
    rc = CmdImport(config, collection, not no_private, input_path).run_command()
    if rc != 0:
        exit(rc)



@app.command("serve")
@click.option("--host", required=False, default="0.0.0.0", help="Host to run the app on")
@click.option("--port", required=False, default=4207, help="Port to run the app on", type=click.types.INT)
@click.option("--debug", required=False, is_flag=True, help="Run the app in debug mode")
@pass_config
def cmd_serve(config: CmdLine, host: str, port: int, debug: bool) -> None:
    """Start a Pagemarks backend server"""

    flask_app = Flask(__name__)


    @flask_app.route("/hello")
    def hello_world():
        return "<p>Hello, World!</p>"


    flask_app.run(host, port, debug)



@click.group(cls=FlaskGroup, add_default_commands=False, add_version_option=False)
def cli():
    """Management script for the Wiki application."""
