{#
    pagemarks.org: Free, git-backed, self-hosted bookmarks
    Copyright (c) The Pagemarks contributors

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License, version 3, as published by the Free Software Foundation.
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.  You should have received a copy of the GNU General Public License along with this program.
    If not, see <https://www.gnu.org/licenses/gpl.html>.
    ________________________________________________________________________________________________________________

    README.md of a pagemarks git repository
    This is a genshi new text template. #}\
# my-pagemarks

This is ${username}'s Pagemarks-powered bookmark repository.

[Pagemarks](https://pagemarks.org) is a free tool that provides Git-based, self-hosted bookmarks.
