#
# pagemarks-server - A Pagemarks server for self-hosting
# Copyright (c) The Pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

import os
from importlib.resources import read_binary
from logging import getLogger, Logger
from typing import Optional

import pygit2.repository
from genshi.template import Context, NewTextTemplate
from pygit2 import Repository

from pagemarks.commands.basecmd import PagemarksCommand
from pagemarks.framework.cmdline import CmdLine
from pagemarks.framework.globals import DEFAULT_COLL_NAME
from pagemarks.framework.repo import COLL_CFG_FILENAME, Repo


LOG: Logger = getLogger(__name__)



class CmdInit(PagemarksCommand):
    """
    Initialize a new Pagemarks repository.

    A new repo contains exactly one collection, named either 'main' or something else specified by
    ``config.collection_name``.
    """
    collection: Optional[str]
    user_name: Optional[str]
    user_email: Optional[str]


    def __init__(self, config: CmdLine, collection: Optional[str], user_name: Optional[str], user_email: Optional[str]):
        super().__init__(config, git_needed=True)
        self.collection = collection if collection is not None else DEFAULT_COLL_NAME
        self.user_name = user_name
        self.user_email = user_email


    def invalid_args(self) -> Optional[str]:
        repo_dir = self.locate_repo()
        if not os.path.isdir(repo_dir):
            os.makedirs(repo_dir)
            LOG.info('Created repo directory: ' + repo_dir)
        if os.listdir(repo_dir):
            return 'Directory not empty: ' + repo_dir
        return None


    def locate_repo(self) -> str:
        if self.config.repo_dir is not None:
            return self.config.repo_dir
        else:
            return '.'


    def open_repo(self) -> Repo:
        repo = Repo(None)
        repo.repo_dir = self.locate_repo()
        return repo


    def provide_git_repo(self, repo: Repo) -> None:
        repo.git_repo = pygit2.init_repository(repo.repo_dir, initial_head='main')
        repo.git_remote = False


    def execute(self, repo: Repo):
        self.configure_git_repo(repo.git_repo)
        self.create_readme_md(repo.repo_dir)
        self.create_git_attributes(repo.repo_dir)
        self.create_gitlabci_yaml(repo.repo_dir)
        self.create_initial_coll(repo)
        self.commit_initial(repo.git_repo)


    def configure_git_repo(self, repo: Repository):
        config = repo.config

        # Tell Git to automatically use system-specific line endings.
        config['core.autocrlf'] = 'true'

        # Turn off the CRLF safety check. Since we know that we only have generated JSON files, the potential
        # corruption is impossible. Any warnings would only mislead or annoy users.
        config['core.safecrlf'] = 'false'

        # Turn off rename detection, which would make no sense in our use case, because we compute file names from
        # the bookmark URLs. Therefore, a different file name will *always* indicate a different bookmark, so
        # rename detection could only find false positives by definition.
        config['diff.renames'] = 'false'

        # Rebase local commits onto the pulled branch, so we have a clean git history.
        config['pull.rebase'] = 'true'

        # Set user name and email as specified on command line.
        if self.user_name is not None and len(self.user_name) > 0:
            config['user.name'] = self.user_name
        if self.user_email is not None and len(self.user_email) > 0:
            config['user.email'] = self.user_email


    def create_readme_md(self, repo_dir: str) -> None:
        template_src = self.read_resource('resources/repo-template/README.md')
        username = 'someone' if self.user_name is None or len(self.user_name.strip()) == 0 else self.user_name
        template = NewTextTemplate(template_src, encoding='utf-8')
        readme_md = template.generate(Context(username=username)).render()
        self.write_resource(repo_dir, '../README.md', readme_md)


    @staticmethod
    def create_git_attributes(repo_dir: str):
        content = '* text=auto\n*.sh text eol=lf\n*.bat text eol=crlf\n'
        with open(os.path.join(repo_dir, '.git/info/attributes'), 'w', newline='\n') as attr_file:
            attr_file.write(content)


    def create_initial_coll(self, repo: Repo) -> None:
        coll_dir = os.path.join(repo.repo_dir, self.collection)
        os.makedirs(coll_dir)
        coll_templ_json = self.read_resource('resources/repo-template/collection-template.json')
        self.write_resource(repo.repo_dir, COLL_CFG_FILENAME, coll_templ_json)
        repo.add_collection(coll_dir)


    def create_gitlabci_yaml(self, repo_dir: str) -> None:
        gitlabci_yml = self.read_resource('resources/repo-template/gitlab-ci.yml')
        self.write_resource(repo_dir, '../.gitlab-ci.yml', gitlabci_yml)


    def commit_initial(self, repo: Repository) -> None:
        repo.index.add('README.md')
        repo.index.add('.gitlab-ci.yml')
        repo.index.add(self.collection + '/' + COLL_CFG_FILENAME)
        repo.index.write()
        tree = repo.index.write_tree()
        user = repo.default_signature
        repo.create_commit('HEAD', user, user, 'Initial Commit', tree, [])


    @staticmethod
    def read_resource(path: str) -> str:
        return read_binary('pagemarks', path) \
            .replace(b'\r\n', b'\n') \
            .decode('utf-8')


    def write_resource(self, repo_dir: str, path: str, content: str) -> None:
        coll_name = self.collection
        with open(os.path.join(repo_dir, coll_name, path), 'w') as f:
            f.write(content)
