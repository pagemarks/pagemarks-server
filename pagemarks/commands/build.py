#
# pagemarks-server - A Pagemarks server for self-hosting
# Copyright (c) The Pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

import os
from logging import getLogger, Logger
from typing import Optional
from zipfile import ZIP_DEFLATED, ZipFile
from importlib.metadata import version

from pagemarks.commands.basecmd import PagemarksCommand
from pagemarks.framework.cmdline import CmdLine
from pagemarks.framework.json import JsonTagList, to_json
from pagemarks.framework.repo import Bookmark, Collection, Repo
from pagemarks.framework.util import normalize_date, normalize_tags


LOG: Logger = getLogger(__name__)



class CmdBuild(PagemarksCommand):
    """Build the downloadable bookmark data from a repository"""
    output_dir: str


    def __init__(self, config: CmdLine, output_dir: str):
        super().__init__(config, git_needed=True)
        self.output_dir = output_dir


    def invalid_args(self) -> Optional[str]:
        return None



    def execute(self, repo: Repo):
        LOG.info('Building pagemarks data ...')
        os.makedirs(self.output_dir, exist_ok=True)
        coll_sizes = {}

        for coll in repo.collections:
            LOG.info(f"Collection '{coll.name}':")
            coll.populate()
            if coll.is_empty():
                LOG.info(f"    Collection '{coll.name}' is empty. Skipping.")
                continue
            LOG.info(f"    Read {len(coll.bookmarks)} bookmarks")
            coll_sizes[coll.name] = {
                # TODO add links to collection files, requires option to pass $CI_PAGES_URL
                'numBookmarks': len(coll.bookmarks),
                'sizeBytes': self.write_collection(repo, coll),
                'zippedSizeBytes': self.write_collection_zipped(coll)
            }

        self.write_collection_list(repo, coll_sizes)
        LOG.info('Done.')


    def write_collection(self, repo: Repo, coll: Collection) -> int:
        consolidated: dict = {}
        self.add_header_info(consolidated, repo)
        consolidated['bookmarks'] = {}
        for bm in coll.bookmarks:
            consolidated['bookmarks'][bm.id] = self.prepare_serialization(bm)
        consolidated['bookmarks'] = dict(sorted(consolidated['bookmarks'].items()))

        consolidated_json: str = to_json(consolidated)
        coll_filename = self.get_coll_filename(coll)

        with open(coll_filename, 'w', encoding='utf-8', newline='\n') as coll_outfile:
            coll_outfile.write(consolidated_json)
        LOG.info('    Data file: ' + coll_filename)
        return os.path.getsize(coll_filename)


    def get_coll_filename(self, coll: Collection) -> str:
        return os.path.join(self.output_dir, coll.get_output_json_filename())


    @staticmethod
    def prepare_serialization(bookmark: Bookmark) -> dict:
        record = {}
        normalized_tags = normalize_tags(bookmark.tags)
        if bookmark.name is not None:
            record['name'] = bookmark.name
        record['url'] = bookmark.url
        if len(normalized_tags) > 0:
            record['tags'] = JsonTagList(normalized_tags)
        if bookmark.date_added is not None:
            record['date_added'] = normalize_date(bookmark.date_added)
        if bookmark.notes is not None:
            record['notes'] = bookmark.notes
        return record


    def write_collection_zipped(self, coll: Collection) -> int:
        zip_filename = os.path.join(self.output_dir, coll.get_output_json_filename_zipped())
        coll_filename = self.get_coll_filename(coll)
        with ZipFile(zip_filename, 'w', ZIP_DEFLATED, compresslevel=9) as zipf:
            zipf.write(coll_filename, coll.get_output_json_filename())
        LOG.info('    Data file (zipped): ' + zip_filename)
        return os.path.getsize(zip_filename)


    @staticmethod
    def add_header_info(data: dict, repo: Repo) -> None:
        data['header'] = {}
        data['header']['backendVersion'] = version('pagemarks')
        data['header']['githash'] = str(repo.git_repo.head.target)


    def write_collection_list(self, repo: Repo, coll_sizes: dict) -> None:
        data: dict = {}
        self.add_header_info(data, repo)
        data['collections'] = dict(sorted(coll_sizes.items()))

        list_json: str = to_json(data, 2)
        list_filename = os.path.join(self.output_dir, '_index.json')
        with open(list_filename, 'w', encoding='utf-8', newline='\n') as coll_outfile:
            coll_outfile.write(list_json)
        LOG.info('Overview: ' + list_filename)
