#
# pagemarks-server - A Pagemarks server for self-hosting
# Copyright (c) The Pagemarks contributors
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License, version 3, as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/gpl.html>.
#

import json
import os
from datetime import datetime, timezone
from json import JSONEncoder
from logging import getLogger, Logger
from typing import Optional

from pygit2 import Repository

from pagemarks.commands.basecmd import PagemarksCommand
from pagemarks.commands.locate import CmdLocate
from pagemarks.framework.cmdline import CmdLine
from pagemarks.framework.repo import Bookmark, Repo
from pagemarks.framework.util import normalize_date, normalize_tags, PagemarksAbort, PagemarksError, prompt, \
    PromptAnswer


LOG: Logger = getLogger(__name__)



class JsonTagList:
    tag_list = None


    def __init__(self, tl):
        self.tag_list = tl



class JsonTagListEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, JsonTagList):
            return "##<{}>##".format(obj.tag_list).replace('\'', '"')



class CmdAdd(PagemarksCommand):
    """Add a bookmark to a collection."""
    collection: str
    bm: Optional[Bookmark]


    def __init__(self, config: CmdLine, collection: str, bm: Optional[Bookmark]):
        super().__init__(config, git_needed=True)
        self.collection = collection
        self.bm = bm


    def invalid_args(self) -> Optional[str]:
        return None


    def execute(self, repo: Repo):
        if self.bm is not None:
            record = {}
            if self.bm.name is not None:
                record['name'] = self.bm.name

            record['url'] = self.bm.url

            normalized_tags = normalize_tags(self.bm.tags)
            if len(normalized_tags) > 0:
                record['tags'] = JsonTagList(normalized_tags)

            if self.bm.date_added is not None:
                record['date_added'] = normalize_date(self.bm.date_added)
            else:
                record['date_added'] = normalize_date(datetime.now(timezone.utc))

            if self.bm.notes is not None:
                record['notes'] = self.bm.notes
            self.log_bookmark_data(record)
            self.add_file(repo, record, git=True)


    @staticmethod
    def log_bookmark_data(record: dict) -> None:
        if 'name' in record:
            LOG.debug(f"Bookmark Name: {record['name']}")
        LOG.debug(f"Bookmark URL: {record['url']}")
        if 'tags' in record:
            LOG.debug(f"Bookmark Tags: {', '.join(record['tags'].tag_list)}")
        LOG.debug(f"Bookmark Date: {record['date_added']}")
        if 'notes' in record:
            LOG.debug(f"Bookmark Notes: {record['notes']}")


    @staticmethod
    def clean_nones(record: dict) -> dict:
        """Remove the None values from the given dictionary."""
        return {
            key: val
            for key, val in record.items()
            if val is not None
        }


    def to_json(self, record: dict) -> str:
        doc = json.dumps(self.clean_nones(record), indent=4, cls=JsonTagListEncoder, ensure_ascii=False)
        doc = doc.replace('"##<', '').replace('>##"', '')
        lines = doc.split('\n')
        doc = ''
        for line in lines:
            if line.strip().startswith('"tags":'):
                doc += line.replace('\\"', '"') + '\n'
            else:
                doc += line + '\n'
        return doc


    def add_file(self, repo: Repo, record: dict, git: bool) -> str:
        coll_name = self.collection
        rel_path = os.path.join(coll_name, CmdLocate(self.config, coll_name, record['url'], False).url_to_filename())
        filename = os.path.join(repo.repo_dir, rel_path)
        LOG.debug(f"Bookmark File Name: {filename}")
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        if self.skip_existing_file(filename, record['url']):
            return rel_path

        doc = self.to_json(record)
        with open(filename, 'w', encoding='utf-8') as f:
            f.write(doc)

        if git:
            self.commit_file(repo.git_repo, coll_name, rel_path, record['url'])
        return rel_path


    @staticmethod
    def commit_file(git_repo: Repository, coll_name: str, rel_path: str, url: str) -> None:
        max_url_len = 72 - len(f"Add  (collection: {coll_name})")
        msg_url = url[:max_url_len - 1] + '…' if len(url) > max_url_len else url
        git_repo.index.add(rel_path.replace('\\', '/'))
        commit_msg = f"Add {msg_url} (collection: {coll_name})"
        git_repo.index.write()
        tree = git_repo.index.write_tree()
        user = git_repo.default_signature
        parents = [git_repo[git_repo.head.target]]
        git_repo.create_commit('HEAD', user, user, commit_msg, tree, parents)


    def skip_existing_file(self, filename: str, url: str) -> bool:
        if os.path.isfile(filename):
            if self.config.yes or self.config.overwrite_all_bookmarks:
                return False
            LOG.info('Bookmark exists: ' + url)
            answer = prompt(self.config, 'Overwrite?')
            if answer == PromptAnswer.YES:
                return False
            elif answer == PromptAnswer.NO:
                LOG.info('Skipping.')
                return True
            elif answer == PromptAnswer.ALL:
                self.config.overwrite_all_bookmarks = True
                return False
            elif answer == PromptAnswer.QUIT:
                # TODO git reset --hard HEAD, possibly after another prompt, or check clean at start
                raise PagemarksAbort()
            else:
                raise PagemarksError('Bug: missed branch')
        return False
